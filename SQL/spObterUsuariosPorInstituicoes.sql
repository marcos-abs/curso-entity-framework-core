DELIMITER //
CREATE PROCEDURE spObterUsuariosPorInstituicoes()
BEGIN
	select 
		us.Nome as NomeUsuario,
		us.SobreNome as SobreNomeUsuario,
        inst.Nome as NomeInstituicaoUsuario
		from usuarios us inner join instituicoesensino inst on us.Id=inst.UsuarioId;
END //
