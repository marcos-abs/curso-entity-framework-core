﻿using Microsoft.EntityFrameworkCore;
using Switch.Domain.Entities;
using Switch.Infra.Data.Config;

namespace Switch.Infra.Data.Context {
    public class SwitchContext : DbContext {
    
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Postagem> Postagens { get; set; }
        public DbSet<StatusRelacionamento> StatusRelacionamento { get; set; } // no singular mesmo (definido que o usuário terá somente um tipo de relacionamento.
        public DbSet<Grupo> Grupos { get; set; }
        public DbSet<Identificacao> Identificacao { get; set; } // no singular mesmo, por conta do relacionamento de um-para-um.
        public DbSet<UsuarioGrupo> UsuarioGrupos { get; set; }
        public DbSet<Amigo> Amigos { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<InstituicaoEnsino> InstituicoesEnsino { get; set; }
        public DbSet<LocalTrabalho> LocaisTrabalho { get; set; }
        public DbSet<ProcurandoPor> ProcurandoPor { get; set; }

        public SwitchContext(DbContextOptions options) : base(options) {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.ApplyConfiguration(new UsuarioConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new PostagemConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new GrupoConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new UsuarioGrupoConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new AmigoConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new ComentarioConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new StatusRelacionamentoConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).
            modelBuilder.ApplyConfiguration(new ProcurandoPorConfiguration()); // existem duas formas de trabalhar com EF Core: por convenção (implícita) e por configuração (explícita).

            base.OnModelCreating(modelBuilder);
        }
    }
}
