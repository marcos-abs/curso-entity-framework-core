﻿namespace SwitchApp.Reports {
    public class UsuarioInstituicaoEnsinoDTO {
        public string NomeUsuario { get; set; }
        public string SobreNomeUsuario { get; set; }
        public string NomeInstituicaoUsuario { get; set; }
    }
}
