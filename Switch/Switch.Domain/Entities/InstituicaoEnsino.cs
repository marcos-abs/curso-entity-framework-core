﻿using System;

namespace Switch.Domain.Entities {
    public class InstituicaoEnsino {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public virtual Usuario usuario { get; set; }
        public string Nome { get; set; }
        public DateTime? AnoFormacao { get; set; } // este campo pode ser nulo, por conta de que não existem validações requisitando ele.
    }
}
