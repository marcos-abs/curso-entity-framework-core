﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Switch.Domain.Enums {
    public enum TipoDocumentoEnum {
        Indefinido = 1,
        CPF = 2,
        RG = 3
    }
}
