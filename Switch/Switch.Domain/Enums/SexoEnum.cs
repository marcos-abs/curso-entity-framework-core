﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Switch.Domain.Enums {
    public enum SexoEnum {
        Indefinido = 1,
        Feminino = 2,
        Masculino = 3
    }
}
